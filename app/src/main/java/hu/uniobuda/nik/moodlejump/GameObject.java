package hu.uniobuda.nik.moodlejump;

import android.graphics.Rect;

/**
 * Created by Kesztike95 on 2017. 11. 05..
 */

public abstract class GameObject {

    protected float x;
    protected float y;
    protected int dx;

    public int getDx() {
        return dx;
    }
    public void setDx(int dx) {
        this.dx = dx;
    }

    protected int width;
    protected int height;

    public void setX(float x) {
        this.x = x;
    }
    public void setY(float y) {
        this.y = y;
    }

    public float getX() {
        return x;
    }
    public float getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public Rect getBounds() {
        return new Rect((int) x, (int) y, (int) x + width, (int) y + height);
    }
}
