package hu.uniobuda.nik.moodlejump;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.List;


public class GamePanel extends SurfaceView implements SurfaceHolder.Callback {

    int WIDTH;
    int HEIGHT;
    int initialY;
    float dY;
    int bgW;
    int bgH;
    int bgScroll;
    int dBgY; // background speed
    Bitmap bg, bgReverse;
    boolean reverseBackgroundFirst;
    float acc;
    int angle;
    boolean ready = false;

    //Measure frames per second
    long now;
    int framesCount = 0;
    int framesCountAvg = 0;
    long framesTimer = 0;
    Paint fpsPaint = new Paint();


    //Frame speed
    long timeNow;
    long timePrevFrame = 0;
    long timeDelta;

    List<Bitmap> players = new ArrayList<Bitmap>();
    List<Credit> credits = new ArrayList<Credit>();
    //List<Platform> platforms = new ArrayList<Platform>();

    public static final int MOVESPEED = 25;
    private MainThread thread;
    //private Background bg;
    private Player player;

    private Credit credit;

    //private Platform platform;

    private float touchX;
    private float touchY;
    private boolean right;
    private int jump = 0;

    public GamePanel(Context context) {

        super(context);

        players.add(BitmapFactory.decodeResource(getResources(), R.drawable.player));
        players.add(BitmapFactory.decodeResource(getResources(), R.drawable.playerleft));
        player = new Player(this.getContext(), getWidth(), getHeight());
        bg = BitmapFactory.decodeResource(getResources(), R.drawable.bg);

        credit = new Credit(this.getContext(), getHeight(), getWidth());
        credits.add(credit);

        //platform = new Platform(this.getContext(),getHeight(),getWidth());
        //platforms.add(platform);

        reverseBackgroundFirst = false;

        acc = 0.2f;
        dY = 0;
        initialY = 100;
        angle = 0;
        bgScroll = 0;
        dBgY = 0;

        fpsPaint.setTextSize(30);

        //Kirajzolás
        setWillNotDraw(false);
        getHolder().addCallback(this);
        thread = new MainThread(getHolder(), this);

        setFocusable(true);
        setClickable(true);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        WIDTH = w;
        HEIGHT = h;

        bg = Bitmap.createScaledBitmap(bg, w, h, true);
        bgW = bg.getWidth();
        bgH = bg.getHeight();

        Matrix matrix = new Matrix();
        matrix.setScale(-1, 1);
        bgReverse = Bitmap.createBitmap(bg, 0, 0, bgW, bgH, matrix, true);

        player.setX((WIDTH / 2) - (player.width / 2));
        player.setY(HEIGHT - 150);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) { }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        thread.setRunning(false);
        while (retry) {
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        thread = new MainThread(getHolder(), this);
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public synchronized boolean onTouchEvent(MotionEvent event) {

        if (!player.isJumping()) {
            touchX = event.getX();
            touchY = event.getY();

            if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
                if (!ready) {
                    ready = true;
                    dBgY = 4;
                }
                if (event.getX() > WIDTH / 2) {
                    player.setJumping(true);
                    player.setPlayer(players.get(0));
                    right = true;
                    invalidate();
                } else {
                    player.setJumping(true);
                    player.setPlayer(players.get(1));
                    right = false;
                    invalidate();
                }
            }
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            dY = 0;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onDraw(Canvas canvas) {

        //Draw scrolling background
        Rect fromRect1 = new Rect(0, 0, bgW, bgH - bgScroll);
        Rect toRect1 = new Rect(0, bgScroll, bgW, bgH);

        Rect fromRect2 = new Rect(0, bgH - bgScroll, bgW, bgH);
        Rect toRect2 = new Rect(0, 0, bgW, bgScroll);

        if (!reverseBackgroundFirst) {
            canvas.drawBitmap(bg, fromRect1, toRect1, null);
            canvas.drawBitmap(bgReverse, fromRect2, toRect2, null);
        } else {
            canvas.drawBitmap(bg, fromRect2, toRect2, null);
            canvas.drawBitmap(bgReverse, fromRect1, toRect1, null);
        }

        //Next value for the background's position
        if ((bgScroll += dBgY) > bgH) {
            bgScroll = 0;
            reverseBackgroundFirst = !reverseBackgroundFirst;
        }

        if (!player.isJumping() && ready == true)
            player.setY((int) player.getY() + 12);
        invalidate();
        if (player.isJumping()) {
            if (right)
                Jump(WIDTH / 25, WIDTH / 10, WIDTH / 10);
            else
                Jump(-WIDTH / 25, WIDTH / 10, WIDTH / 10);
        }

        // Kredit
        if (ready) {
            credit.setY((int) credit.getY() + 8);

            credit.setX((int) credit.getX() + credit.getDx());
            if (credit.getY() > canvas.getHeight()) {
                credit.setY(0);
            }
            if (credit.getX() > canvas.getWidth() || credit.getX() < 0)
                credit.setDx((int) (credit.getDx() * -1));

            if(Rect.intersects(credit.getBounds(), player.getBounds())) {
                credit.createPos();
                player.setCredits(player.getCredits() + credit.getValue());
            }

            credit.draw(canvas);
        }

        //platform.draw(canvas);
        //platform.setY(credit.getY()+8);

        canvas.drawText("Kreditek: " + String.valueOf(player.getCredits()), 20, 50, fpsPaint);
        player.draw(canvas);

        canvas.save();
        now = System.currentTimeMillis();
        framesCount++;
        if (now - framesTimer > 1000) {
            framesTimer = now;
            framesCountAvg = framesCount;
            framesCount = 0;
        }
    }

    public void Jump(int oldalra, int fel, int egyeb) {

        jump++;
        if (jump < 5) {
            if (player.getY() > 0) {
                if (player.getX() < WIDTH - player.getWidth()) {
                    if (player.getX() > 0) {
                        player.setX(player.getX() + oldalra);
                        player.setY(player.getY() - fel);
                        invalidate();
                    } else {
                        player.setY(player.getY() - egyeb);
                        player.setX(player.getX() + 8);
                    }
                } else {
                    player.setY(player.getY() - egyeb);
                    player.setX(player.getX() - 8);
                }
            } else {
                player.setY(player.getY() + egyeb);
            }
        } else if (jump < 5) {
            if (player.getY() > 0) {
                if (player.getX() < WIDTH - player.getWidth()) {
                    player.setX(player.getX() + oldalra);
                    invalidate();
                } else {
                    player.setY(player.getY());
                }
            }
        } else {
            jump = 0;
            player.setJumping(false);
        }
        invalidate();
    }

    class MainThread extends Thread {
        private SurfaceHolder surfaceHolder;
        private GamePanel gamePanel;
        private boolean run = false;

        public MainThread(SurfaceHolder surfaceHolder, GamePanel gamePanel) {
            this.surfaceHolder = surfaceHolder;
            this.gamePanel = gamePanel;
        }

        public void setRunning(boolean run) {
            this.run = run;
        }

        @Override
        public void run() {
            Canvas c;
            while (run) {
                c = null;

                //limit frame rate to max 60fps
                timeNow = System.currentTimeMillis();
                timeDelta = timeNow - timePrevFrame;

                if (timeDelta < 16) {
                    try {
                        Thread.sleep(16 - timeDelta);
                    } catch (InterruptedException e) { }
                }
                timePrevFrame = System.currentTimeMillis();

                try {
                    c = surfaceHolder.lockCanvas(null);
                    synchronized (surfaceHolder) {
                        //call methods to draw and process next fame
                        gamePanel.onDraw(c);
                    }
                } finally {
                    if (c != null) {
                        surfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
        }
    }
}


