package hu.uniobuda.nik.moodlejump;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import java.util.Random;

/**
 * Created by Rúzsa Róbert on 2017. 12. 10..
 */

public class Platform extends GameObject {

    private Bitmap platform;
    int upperBound = 400;
    int lowerBound = 20;
    public static Random r = new Random();

    public Platform(Context context, float w, float h) {
        createPos();
        platform = BitmapFactory.decodeResource(context.getResources(), R.drawable.platform);
        width = platform.getWidth();
        height = platform.getHeight();
        dx = 3;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(platform, x, y, null);
    }

    public void createPos() {
        x = r.nextInt(upperBound - lowerBound) + lowerBound;
        y = r.nextInt(upperBound - lowerBound) + lowerBound;
        this.setX(x);
        this.setY(y);
    }
}
