package hu.uniobuda.nik.moodlejump;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import java.util.Random;

/**
 * Created by Róbert Rúzsa on 2017. 12. 10..
 */

public class Credit extends GameObject {

    private int value;
    private Bitmap credit;
    int upperBound = 240;
    int lowerBound = 20;

    public int getValue() {
        return value;
    }

    public static Random r = new Random();

    public Credit(Context context, float w, float h) {
        value = r.nextInt(6 - 2) + 2;
        credit = BitmapFactory.decodeResource(context.getResources(), R.drawable.credit);
        width = credit.getWidth();
        height = credit.getHeight();
        dx = 3;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(credit, x, y, null);
    }

    public void createPos() {
        x = r.nextInt(upperBound - lowerBound) + lowerBound;
        this.setX(x);
        this.setY(0);
    }
}
