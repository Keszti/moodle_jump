package hu.uniobuda.nik.moodlejump;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.LevelListDrawable;
import android.view.animation.Animation;


public class Player extends GameObject {

    private Bitmap player;
    private boolean jumping;
    private int credits;

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public int getCredits() {
        return credits;
    }

    public Player(Context context, float w, float h) {

        player = BitmapFactory.decodeResource(context.getResources(), R.drawable.player);
        credits = 0;
        width = player.getWidth();
        height = player.getHeight();
        x=w/3-100;
        y=h/5;
    }

    public void setPlayer(Bitmap player) {
        this.player = player;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public void draw(Canvas canvas){
        canvas.drawBitmap(player,x,y,null);
    }


 }

