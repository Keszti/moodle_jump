package hu.uniobuda.nik.moodlejump;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Pryma on 2017. 12. 09..
 */
@Entity(tableName = "user")
public class User {

    @PrimaryKey @NonNull
    public String name;

    public int semester;
    public int credits;

}
